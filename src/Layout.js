import {Outlet} from "react-router-dom"
import MenuBar from "./components/main_menu/MenuBar";

export default function Layout(){
  return (
    <>
      <MenuBar/>
      <Outlet/>
    </>
  )
}

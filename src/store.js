import {configureStore} from '@reduxjs/toolkit';
import consumptionRowReducer from "./components/slices/consumpionRowsSlice";
import totalMonthReducer from "./components/slices/totalMonthSlice";
import totalSalaryReducer from "./components/slices/salaryMonthSlice";
import diagramDataReducer from "./components/slices/diagramDataSlice";
import lastFiveReducer from "./components/slices/lastFiveSlice"
import consumptionDateReducer from "./components/slices/consumtionDateSlice"

export const store = configureStore({
  reducer: {
    consumptionRow: consumptionRowReducer,
    totalMonth: totalMonthReducer,
    salaryMonth: totalSalaryReducer,
    diagramData: diagramDataReducer,
    lastFiveData: lastFiveReducer,
    consumptionDate: consumptionDateReducer,
  },
})

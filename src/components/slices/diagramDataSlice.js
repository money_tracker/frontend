import {createSlice} from "@reduxjs/toolkit";
import {round} from "../consumption/SpentTable";

const initialState = {
  value: [],
}

const sortFunc = (prev, cur) => {
  if (prev.value < cur.value) {
    return -1;
  }
  if (prev.value > cur.value) {
    return 1;
  }
  return 0;
}

export const diagramDataSlice = createSlice({
  name: 'diagramData',
  initialState,
  reducers: {
    setDiagramData: (state, data) => {
      state.value = [];
      data.payload.map(val => {
        const index = state.value.findIndex(elem => elem.name === val.category.category);
        if( index === -1) {
          state.value = [...state.value, {name: val.category.category, value: val.amount}].sort(sortFunc);
        } else {
          let newAmount = round(state.value[index].value + val.amount);
          state.value[index].value = newAmount;
          state.value = [...state.value].sort(sortFunc);
        }
      });
    },
    addDiagramData: (state, data) => {
      const payload = data.payload;
      const index = state.value.findIndex(elem => elem.name === payload.category.category);
      if( index === -1) {
        state.value = [...state.value, {name: payload.category.category, value: payload.amount}].sort(sortFunc);
      } else {
        let newAmount = round(state.value[index].value + payload.amount);
        state.value[index].value = newAmount;
        state.value = [...state.value].sort(sortFunc);
      }
    },
    deleteDiagramData: (state, data) => {
      const payload = data.payload;
      const index = state.value.findIndex(elem => elem.name === payload.category);
      state.value[index].value -= payload.amount;
      if (state.value[index].value === 0) {
        delete state.value[index];
      }
    },
    updateDiagramData: (state, data) => {
      const payload = data.payload;
      const indexPrevCategory = state.value.findIndex(elem => elem.name === payload.prevCategory);
      const indexCurCategory = state.value.findIndex(elem => elem.name === payload.curCategory);
      if (indexPrevCategory === indexCurCategory) {
        state.value[indexPrevCategory].value = state.value[indexPrevCategory].value - payload.sub + payload.add;
      } else {
        state.value[indexPrevCategory].value -= payload.sub;
        if (indexCurCategory !== -1) {
          state.value[indexCurCategory].value += payload.add;
        } else {
          state.value.push({name: payload.curCategory, value: payload.add});
        }
      }
    },
  },
})

export const {setDiagramData, addDiagramData, deleteDiagramData, updateDiagramData} = diagramDataSlice.actions

export default diagramDataSlice.reducer

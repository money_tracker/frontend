import {createSlice} from "@reduxjs/toolkit";

const initialState = {
  value: 0,
}

export const totalSalarySlice = createSlice({
  name: 'salaryMonth',
  initialState,
  reducers: {
    setSalary: (state, data) => {
      if (data.payload !== undefined) {
        state.value = data.payload
      } else {
        state.value = 0
      }
    }
  },
})

export const {setSalary} = totalSalarySlice.actions

export default totalSalarySlice.reducer

import {createSlice} from "@reduxjs/toolkit";

const initialState = {
  value: 0,
}

export const totalMonthSlice = createSlice({
  name: 'totalMonth',
  initialState,
  reducers: {
    addTotal: (state, data) => {
      state.value = state.value + Number(data.payload)
    },
    setTotal: (state, data) => {
      state.value = data.payload
    },
    subTotal: (state, data) => {
      state.value = state.value - Number(data.payload)
    },
    updateTotal: (state, data) => {
      const payload = data.payload
      state.value = state.value - Number(payload.sub) + Number(payload.add)
    }
  },
})

export const {addTotal, setTotal, subTotal, updateTotal} = totalMonthSlice.actions

export default totalMonthSlice.reducer

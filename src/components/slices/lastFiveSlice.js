import {createSlice} from "@reduxjs/toolkit";

const initialState = {
  value: [],
}

export const lastFiveSlice = createSlice({
  name: 'lastFive',
  initialState,
  reducers: {
    setLastFive: (state, data) => {
      state.value = data.payload;
    },
  },
})

export const {setLastFive} = lastFiveSlice.actions

export default lastFiveSlice.reducer

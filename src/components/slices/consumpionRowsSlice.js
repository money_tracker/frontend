import {createSlice} from '@reduxjs/toolkit'

const initialState = {
  value: {},
}

export const consumptionRowSlice = createSlice({
  name: 'consumptionRow',
  initialState,
  reducers: {
    addRow: (state, row) => {
      const payload = row.payload;
      const category = payload.category.category;
      const {id, amount} = payload;

      if (state.value[category] !== undefined) {
        state.value = {...state.value, [category]: [...state.value[category], {id, amount}]};
      } else {
        state.value = {...state.value, [category]: [{id, amount}]};
      }
    },
    addRows: (state, res) => {
      const rebuiltResponse = {};

      res.payload.map(val => {
        const category = val.category.category;
        const {id, amount} = val;
        rebuiltResponse[category] = rebuiltResponse[category] === undefined ? [{
          id,
          amount
        }] : [...rebuiltResponse[category], {id, amount}];
      });

      state.value = rebuiltResponse;
    },
    deleteRow: (state, res) => {
      const category = res.payload.category;
      const currentItems = state.value[category];
      const index = currentItems.findIndex(elem => elem.id === res.payload.id);
      currentItems.splice(index, 1);
      if (currentItems.length > 0) {
        state.value[category] = [...currentItems];
      } else {
        delete state.value[category];
      }
    },
    updateRow: (state, data) => {
      const payload = data.payload;
      const response = payload.response;

      const prevCategory = payload.prevCategory;
      const currentCategory = response.category.category;

      const {id, amount} = response;
      const currentItem = state.value[currentCategory];
      const prevItem = state.value[prevCategory];

      if (currentItem === undefined) {
        state.value[currentCategory] = [{id, amount}];
        const index = prevItem.findIndex(elem => elem.id === response.id);
        const prevItemsArray = state.value[prevCategory];
        prevItemsArray.splice(index, 1);
        state.value[prevCategory] = [...prevItemsArray];
      } else {
        state.value[currentCategory] = [...currentItem, {id, amount}];
        const prevItemsArray = state.value[prevCategory];

        const index = prevItem.findIndex(elem => elem.id === response.id);
        prevItemsArray.splice(index, 1);
        if (prevItemsArray.length > 0) {
          state.value[prevCategory] = [...prevItemsArray];
        } else {
          delete state.value[prevCategory];
        }
      }
    }
  },
})

export const {addRow, addRows, deleteRow, updateRow} = consumptionRowSlice.actions

export default consumptionRowSlice.reducer

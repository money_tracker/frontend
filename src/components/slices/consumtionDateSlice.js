import {createSlice} from "@reduxjs/toolkit";

export const getFormattedDate = (date) => {
  return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
}

const initialState = {
  value: getFormattedDate(new Date()),
}

export const consumptionDateSlice = createSlice({
  name: 'consumptionDate',
  initialState,
  reducers: {
    setConsumptionDate: (state, data) => {
      state.value = data.payload;
    },
  },
})

export const {setConsumptionDate} = consumptionDateSlice.actions

export default consumptionDateSlice.reducer

import {Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";
import {useEffect} from "react";
import {axiosInstance, URLs} from "../../index";
import {getAuthHeader} from "../authentification/utils";
import {useDispatch, useSelector} from "react-redux";
import {setLastFive} from "../slices/lastFiveSlice";

export default function LastFiveTable() {
  const authHeader = getAuthHeader();
  const lastFiveData = useSelector((state) => state.lastFiveData.value);
  const date = useSelector((state) => state.consumptionDate.value);
  const dispatch = useDispatch();

  useEffect(() => {
    axiosInstance.get(`${URLs.SPENT_ITEM}/diagram?month_date=${date}`, {headers: {...authHeader}}).then(res => {
      dispatch(setLastFive(res.data.slice(0, 5)));
    });
  }, [])

  return (
    <div>
      <TableContainer sx={{maxHeight: 700}}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <TableCell align="center" colSpan={3}>Last five</TableCell>
            </TableRow>
            <TableRow>
              <TableCell align="center" colSpan={2}>Date</TableCell>
              <TableCell align="center" colSpan={2}>Amount</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              lastFiveData.map((elem, index) => {
                return (
                  <TableRow key={index}>
                    <TableCell align="center" colSpan={2}>
                      {elem.create_on}
                    </TableCell>
                    <TableCell align="center" colSpan={2}>
                      {elem.amount}
                    </TableCell>
                  </TableRow>
                )
              })
            }
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  )
}

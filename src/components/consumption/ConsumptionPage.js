import {Container} from "@mui/material";
import MoneySendForm from "../money_send_form/MoneySendForm";
import SpentTable from "./SpentTable";
import {useEffect} from "react";
import {axiosInstance, URLs} from "../../index";
import {useNavigate} from "react-router-dom";
import {getAuthHeader, logout} from "../authentification/utils";
import {useDispatch, useSelector} from "react-redux";
import {addRows} from "../slices/consumpionRowsSlice";
import {setTotal} from "../slices/totalMonthSlice";
import {setSalary} from "../slices/salaryMonthSlice";
import ConsumptionDiagram from "../consumption_diagram/ConsumptionDiagram";
import LastFiveTable from "../last_five_items/LastFiveTable";


export default function ConsumptionPage() {
  const date = useSelector((state) => state.consumptionDate.value);

  const dispatch = useDispatch();

  const navigation = useNavigate();
  const authHeader = getAuthHeader();

  useEffect(() => {
    axiosInstance.get(`${URLs.SPENT_ITEM}?filter_date=${date}`, {headers: {...authHeader}}).then(res => {
      dispatch(addRows(res.data));
    }).catch(e => logout(navigation, {status: e.response.status}));

    axiosInstance.get(`${URLs.INCOME}?month_date=${date}`, {headers: {...authHeader}}).then(res => {
      dispatch(setSalary(res.data?.salary));
    })

    axiosInstance.get(`${URLs.SPENT_ITEM}/month-total?today=${date}`, {headers: {...authHeader}}).then(res => {
      dispatch(setTotal(res.data));
    }).catch(e => logout(navigation, {status: e.response.status}));
  }, []);

  return (
    <Container maxWidth="xl" style={{display: "flex", alignItems: "flex-start"}}>
      <Container maxWidth="lg" style={{marginTop: "20px"}}>
        <MoneySendForm/>
        <LastFiveTable/>
      </Container>
      <Container maxWidth="lg" style={{marginTop: "20px"}}>
        <SpentTable/>
        <ConsumptionDiagram/>
      </Container>
    </Container>
  )
}

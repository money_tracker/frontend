import {Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@mui/material";
import Button from "@mui/material/Button";
import {axiosInstance, URLs} from "../../index";
import {useSnackbar} from "notistack";
import {getAuthHeader} from "../authentification/utils";
import {useDispatch} from "react-redux";
import {deleteRow} from "../slices/consumpionRowsSlice";
import {subTotal} from "../slices/totalMonthSlice";
import {deleteDiagramData} from "../slices/diagramDataSlice";


export default function DeleteDialog(props) {
  const dispatch = useDispatch();

  const {enqueueSnackbar} = useSnackbar();
  const authHeader = getAuthHeader();

  const onSubmit = e => {
    e.preventDefault()
    axiosInstance.delete(`${URLs.SPENT_ITEM}/${props.spentData.id}`, {headers: {...authHeader}}).then(() => {
      dispatch(deleteRow({id: props.spentData.id, category: props.category}));
      dispatch(subTotal(props.spentData.amount));
      dispatch(deleteDiagramData({amount: props.spentData.amount, category: props.category}));
      enqueueSnackbar(`Item "${props.spentData.id}" has been deleted`, {variant: "success"});
    });
    props.onClose();
  };

  return (
    <Dialog open={props.open} onClose={props.onClose}>
      <form onSubmit={onSubmit}>
        <DialogTitle>Delete</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Do you want to delete {props.spentData.amount} item?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => props.onClose()}>Cancel</Button>
          <Button type="submit">Delete</Button>
        </DialogActions>
      </form>
    </Dialog>
  );
}

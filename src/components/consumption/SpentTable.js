import React, {Fragment,useState} from "react";
import {
  Collapse,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField
} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import {Create, Delete} from "@mui/icons-material";
import {DesktopDatePicker} from "@mui/x-date-pickers";
import {LocalizationProvider} from '@mui/x-date-pickers/LocalizationProvider';
import {AdapterDateFns} from '@mui/x-date-pickers/AdapterDateFns';
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import Box from "@mui/material/Box";
import DeleteDialog from "./DeleteDialog";
import ChangeDialog from "./ChangeDialog";
import {useDispatch, useSelector} from "react-redux";
import {getFormattedDate, setConsumptionDate} from "../slices/consumtionDateSlice";
import {axiosInstance, URLs} from "../../index";
import {addRows} from "../slices/consumpionRowsSlice";
import {getAuthHeader, logout} from "../authentification/utils";
import {setDiagramData} from "../slices/diagramDataSlice";
import {setLastFive} from "../slices/lastFiveSlice";
import {setSalary} from "../slices/salaryMonthSlice";
import {setTotal} from "../slices/totalMonthSlice";
import {useNavigate} from "react-router-dom";

export const round = (numberToRound) => {
  return Math.round(numberToRound * 100) / 100;
}

export default function SpentTable() {
  const rows = useSelector((state) => state.consumptionRow.value);
  const totalMonth = useSelector((state) => state.totalMonth.value);
  const salaryMonth = useSelector((state) => state.salaryMonth.value);
  const date = useSelector((state) => state.consumptionDate.value);

  const dispatch = useDispatch();
  const authHeader = getAuthHeader();
  const navigation = useNavigate();

  const calculateDayConsumption = () => {
    let sum = 0;
    Object.values(rows).map(row => {
      row.map(value => {
        sum += value.amount;
      });
    });
    return round(sum);
  }

  const calculateRest = () => {
    let rest = salaryMonth - totalMonth;
    return round(rest);
  }

  const setFilterDate = (date) => {
    const queryDate = getFormattedDate(date);
    axiosInstance.get(`${URLs.SPENT_ITEM}?filter_date=${queryDate}`, {headers: {...authHeader}}).then(res => {
      dispatch(addRows(res.data));
    });
    dispatch(setConsumptionDate(queryDate));
    axiosInstance.get(`${URLs.SPENT_ITEM}/diagram?month_date=${queryDate}`, {headers: {...authHeader}}).then(res => {
      dispatch(setDiagramData(res.data));
      dispatch(setLastFive(res.data.slice(0, 5)));
    });
    axiosInstance.get(`${URLs.INCOME}?month_date=${queryDate}`, {headers: {...authHeader}}).then(res => {
      dispatch(setSalary(res.data?.salary));
    });
    axiosInstance.get(`${URLs.SPENT_ITEM}/month-total?today=${queryDate}`, {headers: {...authHeader}}).then(res => {
        dispatch(setTotal(res.data));
    }).catch(e => logout(navigation, {status: e.response.status}));
  };

  return (
    <Paper sx={{width: '100%'}} style={{marginTop: "18px"}}>
      <TableContainer sx={{maxHeight: 700}}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <TableCell align="center" colSpan={4}>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                  <DesktopDatePicker
                    label="Date desktop"
                    inputFormat="dd/MM/yyyy"
                    value={date}
                    onChange={setFilterDate}
                    renderInput={(params) => <TextField {...params} />}
                  />
                </LocalizationProvider>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              Object.keys(rows).map((category, rowIndex) => {
                return <Row category={category} key={rowIndex} date={date}/>
              })
            }
            <TableRow>
              <TableCell rowSpan={3}/>
              <TableCell colSpan={2}>Total day</TableCell>
              <TableCell align="right">{calculateDayConsumption()}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell colSpan={2}>Total spent month</TableCell>
              <TableCell align="right">{round(totalMonth)}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell colSpan={2}>Left month</TableCell>
              <TableCell align="right">{calculateRest()}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
}


function Row(props) {
  const rows = useSelector((state) => state.consumptionRow.value);

  const [open, setOpen] = useState(false);

  const [spentData, setSpentData] = useState({});
  const [openChange, setOpenChange] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);

  const onOpenChange = (data) => {
    setSpentData(data);
    setOpenChange(true);
  };

  const onCloseChange = () => {
    setOpenChange(false);
  };

  const onOpenDelete = (data) => {
    setSpentData(data);
    setOpenDelete(true);
  };

  const onCloseDelete = () => {
    setOpenDelete(false);
  };

  return (
    <Fragment>
      <TableRow>
        <TableCell>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUpIcon/> : <KeyboardArrowDownIcon/>}
          </IconButton>
        </TableCell>
        <TableCell align="left">{props.category}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{paddingBottom: 0, paddingTop: 0}} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box sx={{margin: 1}}>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell>Amount</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {
                    rows[props.category].map((item, index) => {
                      return <TableRow key={index}>
                        <TableCell>{item.amount}</TableCell>
                        <TableCell align="right" colSpan={1}>
                          <IconButton color="primary" aria-label="upload picture" component="span"
                                      onClick={() => onOpenChange(item, index)}>
                            <Create/>
                          </IconButton>
                          <IconButton color="primary" aria-label="upload picture" component="span"
                                      onClick={() => onOpenDelete(item, index)}>
                            <Delete/>
                          </IconButton>
                        </TableCell>
                      </TableRow>
                    })
                  }
                </TableBody>
              </Table>
              <ChangeDialog open={openChange} onClose={onCloseChange} spentData={spentData} category={props.category} date={props.date}/>
              <DeleteDialog open={openDelete} onClose={onCloseDelete} category={props.category} spentData={spentData}/>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </Fragment>
  )
}

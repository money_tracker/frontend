import {
    Autocomplete,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    TextField
} from "@mui/material";
import Button from "@mui/material/Button";
import {Controller, useForm} from "react-hook-form";
import {axiosInstance, URLs} from "../../index";
import {useState, useEffect, React} from "react";
import {useSnackbar} from "notistack";
import {getAuthHeader, logout} from "../authentification/utils";
import {useNavigate} from "react-router-dom";
import {useDispatch} from "react-redux";
import {deleteRow, updateRow} from "../slices/consumpionRowsSlice";
import {updateTotal} from "../slices/totalMonthSlice";
import {updateDiagramData} from "../slices/diagramDataSlice";
import {LocalizationProvider} from "@mui/x-date-pickers/LocalizationProvider";
import {AdapterDateFns} from "@mui/x-date-pickers/AdapterDateFns";
import {DesktopDatePicker} from "@mui/x-date-pickers";
import {makeStyles} from "@mui/styles";
import {getFormattedDate} from "../slices/consumtionDateSlice";


const useStyles = makeStyles((theme) => ({
    flexDialogContent: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignContent: 'space-around'
    },
}));

export default function ChangeDialog(props) {
    const {register, handleSubmit, reset, control} = useForm({defaultValues: {amount: 0}});
    const {enqueueSnackbar} = useSnackbar();
    const authHeader = getAuthHeader();
    const navigation = useNavigate();

    const dispatch = useDispatch();
    const [categoryList, setCategoryList] = useState([]);
    const [date, setDate] = useState(new Date(props.date));
    const classes = useStyles();

    const onSubmit = data => {
        const setDate = getFormattedDate(date);
        const requestBody = {amount: data.amount, "category_id": data.category?.id, create_on: setDate};
        axiosInstance.put(`${URLs.SPENT_ITEM}/${props.spentData.id}`, requestBody, {headers: {...authHeader}}).then(res => {
            console.log(new Date(res.data.create_on).getDate() === new Date(props.date).getDate())
            if (new Date(res.data.create_on).getDate() === new Date(props.date).getDate()) {
                console.log("Date has not been changed")
                dispatch(updateRow({prevCategory: props.category, response: res.data}));
                dispatch(updateTotal({sub: props.spentData.amount, add: data.amount}));
                dispatch(updateDiagramData({
                    sub: props.spentData.amount,
                    add: data.amount,
                    prevCategory: props.category,
                    curCategory: res.data.category.category
                }));
            } else {
                dispatch(deleteRow({id: props.spentData.id, category: props.category}));
            }
            enqueueSnackbar(`Item "${props.spentData.amount}" has been changed on "${data.amount}"`, {variant: "success"});
        });
        props.onClose();
    };

    useEffect(() => {
        reset({amount: props.spentData.amount});
    }, [reset, props.spentData.amount]);

    useEffect(() => {
        axiosInstance.get(URLs.CATEGORIES, {headers: {...authHeader}}).then(res => {
            setCategoryList(res.data);
        }).catch(e => logout(navigation, {status: e.response.status}));
    }, []);

    return (
        <Dialog open={props.open} onClose={props.onClose}>
            <form onSubmit={handleSubmit(onSubmit)} className={classes.flexDialogContent}>
                <DialogTitle>Change</DialogTitle>
                <DialogContent className={classes.flexDialogContent}>
                    <DialogContentText>
                        Change spent amount
                    </DialogContentText>
                    <TextField
                        margin="dense"
                        label="Spent amount"
                        variant="standard"
                        {...register("amount")}
                    />
                    <Controller
                        render={({field: {onChange, value}}) =>
                            <Autocomplete
                                isOptionEqualToValue={(option, value) => option.id === value.id}
                                value={value}
                                disablePortal
                                defaultValue={null}
                                getOptionLabel={option => option.category}
                                options={categoryList}
                                renderInput={(params) => <TextField {...params} label="Category"/>}
                                onChange={(_, data) => onChange(data)}
                            />
                        }
                        defaultValue={null}
                        name="category"
                        control={control}
                        onChange={([, obj]) => console.log(obj)}
                    />
                    <LocalizationProvider dateAdapter={AdapterDateFns} style={{marginTop: '50px'}}>
                        <DesktopDatePicker
                            label="Date desktop"
                            inputFormat="dd/MM/yyyy"
                            value={date}
                            onChange={setDate}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </LocalizationProvider>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => props.onClose()}>Cancel</Button>
                    <Button type="submit">Change</Button>
                </DialogActions>
            </form>
        </Dialog>
    );
}

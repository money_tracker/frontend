import {useState} from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import {Menu, MenuItem} from "@mui/material";
import {NavLink} from "react-router-dom";
import UserMenu from "./UserMenu";


export default function MenuBar() {
  const [anchorMainMenu, setAnchorMain] = useState(null);
  const openMainMenu = Boolean(anchorMainMenu);

  const handleClickMainMenu = (event) => {
    setAnchorMain(event.currentTarget);
  };

  const handleCloseMainMenu = () => {
    setAnchorMain(null);
  };

  return (
    <Box sx={{flexGrow: 1}}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{mr: 2}}
            onClick={handleClickMainMenu}
          >
            <MenuIcon/>
          </IconButton>
          <Menu
            id="basic-menu"
            anchorEl={anchorMainMenu}
            open={openMainMenu}
            onClose={handleCloseMainMenu}
            MenuListProps={{
              'aria-labelledby': 'basic-button',
            }}
          >
            <MenuItem onClick={handleCloseMainMenu} component={NavLink} to="/">Consumption</MenuItem>
            <MenuItem onClick={handleCloseMainMenu} component={NavLink} to="/settings">Settings</MenuItem>
          </Menu>
          <Typography variant="h6" sx={{flexGrow: 1}}>
            Menu
          </Typography>
          <UserMenu/>
        </Toolbar>
      </AppBar>
    </Box>
  );
}

import IconButton from "@mui/material/IconButton";
import {AccountCircle} from "@mui/icons-material";
import {Menu, MenuItem} from "@mui/material";
import {NavLink, useNavigate} from "react-router-dom";
import {useState} from "react";

export default function UserMenu() {
  const [anchorUserMenu, setAnchorUser] = useState(null);
  const openUserMenu = Boolean(anchorUserMenu);
  const navigate = useNavigate();

  const handleUserMenu = event => {
    setAnchorUser(event.currentTarget);
  };

  const handleCloseUserMenu = () => {
    setAnchorUser(null);
  };

  const logout = () => {
    localStorage.removeItem("token");
    navigate("/");
  };

  return (
    <>
      <IconButton
        size="large"
        aria-label="account of current user"
        aria-controls="menu-appbar"
        aria-haspopup="true"
        onClick={handleUserMenu}
        color="inherit"
      >
        <AccountCircle/>
      </IconButton>
      <Menu
        id="menu-appbar"
        anchorEl={anchorUserMenu}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={openUserMenu}
        onClose={handleCloseUserMenu}
      >
        <MenuItem onClick={handleCloseUserMenu} component={NavLink} to="/user/me">My account</MenuItem>
        <MenuItem onClick={logout}>Logout</MenuItem>
      </Menu>
    </>
  )
}

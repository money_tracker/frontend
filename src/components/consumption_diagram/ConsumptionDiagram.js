import {Pie, PieChart, Tooltip} from "recharts";
import {useEffect} from "react";
import {axiosInstance, URLs} from "../../index";
import {getAuthHeader} from "../authentification/utils";
import {useDispatch, useSelector} from "react-redux";
import {setDiagramData} from "../slices/diagramDataSlice";


export default function ConsumptionDiagram() {
  const authHeader = getAuthHeader();
  const dispatch = useDispatch();
  const diagramData = useSelector((state) => state.diagramData.value);
  const date = useSelector((state) => state.consumptionDate.value);

  useEffect(() => {
    axiosInstance.get(`${URLs.SPENT_ITEM}/diagram?month_date=${date}`, {headers: {...authHeader}}).then(res => {
      dispatch(setDiagramData(res.data));
    });
  }, [])

  return (
    <PieChart width={1000} height={400}>
      <Pie
        dataKey="value"
        data={diagramData}
        cx={500}
        cy={200}
        innerRadius={80}
        outerRadius={150}
        fill="#82ca9d"
        label
      />
      <Tooltip/>
    </PieChart>
  );
}

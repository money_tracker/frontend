import {Container} from "@mui/material";
import CategoryTable from "./CategoryTable";
import CurrencyExchangeForm from "./CurrencyExchangeForm";
import IncomeForm from "./IncomeForm";
import LeftForm from "./LeftForm";

export default function Settings() {
    return (
        <div style={{display: "flex", flexDirection: "row", marginTop: "10px"}}>
            <Container maxWidth={"xs"} style={{display: "flex", flexDirection: "column"}}>
                <CurrencyExchangeForm/>
                <IncomeForm/>
                <LeftForm/>
            </Container>
            <CategoryTable/>
        </div>
    )
}

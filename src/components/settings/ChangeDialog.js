import {Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField} from "@mui/material";
import Button from "@mui/material/Button";
import {useForm} from "react-hook-form";
import {axiosInstance, URLs} from "../../index";
import {useEffect} from "react";
import {useSnackbar} from "notistack";
import {getAuthHeader} from "../authentification/utils";


export default function ChangeDialog(props) {
  const {register, handleSubmit, reset} = useForm({defaultValues: {category: ""}});
  const {enqueueSnackbar} = useSnackbar();
  const authHeader = getAuthHeader();

  const onSubmit = data => {
    axiosInstance.put(`${URLs.CATEGORIES}/${props.categoryId}`, data, {headers: {...authHeader}}).then(() => {
      props.setColumns(oldArray => {
        oldArray[props.index] = data;
        return [...oldArray];
      });
      enqueueSnackbar(`Category "${props.category}" has been changed on "${data.category}"`, {variant: "success"});
    });
    props.onClose();
  };

  useEffect(() => {
    reset({category: props.category});
  }, [reset, props.category]);

  return (
    <Dialog open={props.open} onClose={props.onClose}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <DialogTitle>Change</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Change category name
          </DialogContentText>
          <TextField
            margin="dense"
            label="Category"
            variant="standard"
            {...register("category")}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => props.onClose()}>Cancel</Button>
          <Button type="submit">Change</Button>
        </DialogActions>
      </form>
    </Dialog>
  );
}

import {Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@mui/material";
import Button from "@mui/material/Button";
import {axiosInstance, URLs} from "../../index";
import {useSnackbar} from "notistack";
import {getAuthHeader} from "../authentification/utils";


export default function DeleteDialog(props) {
  const {enqueueSnackbar} = useSnackbar();
  const authHeader = getAuthHeader();

  const onSubmit = e => {
    e.preventDefault()
    axiosInstance.delete(`${URLs.CATEGORIES}/${props.categoryId}`, {headers: {...authHeader}}).then(() => {
      props.setColumns(oldArray => {
        oldArray.splice(props.index, 1);
        return [...oldArray];
      });
      enqueueSnackbar(`Category "${props.category}" has been deleted`, {variant: "success"});
    });
    props.onClose();
  };

  return (
    <Dialog open={props.open} onClose={props.onClose}>
      <form onSubmit={onSubmit}>
        <DialogTitle>Delete</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Do you want to delete {`"${props.category}"`} category?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => props.onClose()}>Cancel</Button>
          <Button type="submit">Delete</Button>
        </DialogActions>
      </form>
    </Dialog>
  );
}

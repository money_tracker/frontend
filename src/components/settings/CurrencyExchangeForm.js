import {Container, TextField} from "@mui/material";
import Button from "@mui/material/Button";
import {useForm, useFormState} from "react-hook-form";
import {useEffect, useState} from "react";
import {axiosInstance, URLs} from "../../index";
import {useSnackbar} from "notistack";
import {getAuthHeader} from "../authentification/utils";
import Typography from "@mui/material/Typography";

export default function CurrencyExchangeForm() {
    const {register, handleSubmit, reset, control} = useForm({
        defaultValues: {euro: 0, usd: 0, eurousd: 0}
    });

    const {dirtyFields} = useFormState({
        control
    });

    const [incomingData, setIncomingData] = useState([]);
    const {enqueueSnackbar} = useSnackbar();
    const authHeader = getAuthHeader();


    useEffect(() => {
        axiosInstance.get(URLs.SETTINGS, {headers: {...authHeader}}).then(res => {
            setIncomingData(res.data);
            let groupData = {};
            res.data.forEach(val => {
                    groupData[val.name] = val.value;
                }
            );
            reset(groupData);
        });
    }, [reset]);

    const onSubmit = data => {
        for (let key in dirtyFields) {
            const changedData = incomingData.find(({name}) => name === key);
            changedData.value = data[key];
            axiosInstance.put(`${URLs.SETTINGS}/${changedData.id}`, changedData, {headers: {...authHeader}}).then(() => {
                enqueueSnackbar(`${key.toUpperCase()} field has been changed`, {variant: "success"});
            });
        }
    };

    return (
        <Container maxWidth={"xs"}>
            <Typography style={{display: "flex", justifyContent: "center"}}>
                Currency exchange
            </Typography>
            <form onSubmit={handleSubmit(onSubmit)} style={{
                height: "265px",
                display: "flex",
                alignItems: "center",
                justifyContent: "space-around",
                flexDirection: "column"
            }}>
                <TextField id="outlined-basic" label="Euro" variant="outlined" {...register("euro")}/>
                <TextField id="outlined-basic" label="USD" variant="outlined" {...register("usd")}/>
                <TextField id="outlined-basic" label="Euro/USD" variant="outlined" {...register("eurousd")}/>
                <Button variant="outlined" type={"submit"}>Submit</Button>
            </form>
        </Container>
    )
}

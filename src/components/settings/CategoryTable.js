import {Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import {Create, Delete} from "@mui/icons-material";
import ChangeDialog from "./ChangeDialog";
import DeleteDialog from "./DeleteDialog";
import Button from "@mui/material/Button";
import {axiosInstance, URLs} from "../../index";
import {useEffect, useState} from "react";
import {getAuthHeader, logout} from "../authentification/utils";
import {useSnackbar} from "notistack";
import {useNavigate} from "react-router-dom";
import {useForm} from "react-hook-form";

export default function CategoryTable() {
  const {register, handleSubmit} = useForm();

  const [openChange, setOpenChange] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);

  const [columns, setColumns] = useState([]);
  const [category, setCategory] = useState("");
  const [categoryId, setCategoryId] = useState(0);
  const [index, setIndex] = useState(0);

  const {enqueueSnackbar} = useSnackbar();
  const authHeader = getAuthHeader();
  const navigation = useNavigate();

  useEffect(() => {
    axiosInstance.get(URLs.CATEGORIES, {headers: {...authHeader}}).then(res => {
      setColumns(res.data);
    }).catch(e => logout(navigation, {status: e.response.status}));
  }, []);

  const onSubmitCategory = data => {
    axiosInstance.post(URLs.CATEGORIES, data, {headers: {...authHeader}}).then(res => {
      setColumns(oldArr => [...oldArr, res.data]);
      enqueueSnackbar(`Category "${data.category}" has been added`, {variant: "success"});
    }).catch(e => {
      if (e.response.status === 409) {
        enqueueSnackbar(`You already got category "${data}"`, {variant: "warning"});
      }
    });
  };

  const onOpenChange = (category, categoryId, index) => {
    setCategoryId(categoryId);
    setCategory(category);
    setIndex(index);
    setOpenChange(true);
  };

  const onCloseChange = () => {
    setOpenChange(false);
  };

  const onOpenDelete = (category, categoryId, index) => {
    setCategoryId(categoryId);
    setCategory(category);
    setIndex(index);
    setOpenDelete(true);
  };

  const onCloseDelete = () => {
    setOpenDelete(false);
  };

  return (
    <Paper sx={{width: '50%'}} style={{marginTop: "18px", marginRight: "50px"}}>
      <TableContainer sx={{maxHeight: 700}}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <TableCell align="center" colSpan={2}>Categories</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              columns.map((val, ind) => {
                return (
                  <TableRow key={ind}>
                    <TableCell align="left" colSpan={1}>{val.category}</TableCell>
                    <TableCell align="right" colSpan={1}>
                      <IconButton color="primary" aria-label="upload picture" component="span"
                                  onClick={() => onOpenChange(val.category, val.id, ind)}>
                        <Create/>
                      </IconButton>
                      <IconButton color="primary" aria-label="upload picture" component="span"
                                  onClick={() => onOpenDelete(val.category, val.id, ind)}>
                        <Delete/>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                )
              })
            }
          </TableBody>
        </Table>
        <ChangeDialog open={openChange} onClose={onCloseChange}
                      categoryId={categoryId} index={index} category={category} setColumns={setColumns}/>
        <DeleteDialog open={openDelete} onClose={onCloseDelete}
                      categoryId={categoryId} index={index} category={category} setColumns={setColumns}/>
      </TableContainer>
      <form onSubmit={handleSubmit(onSubmitCategory)} style={{
        height: "200px",
        display: "flex",
        alignItems: "center",
        justifyContent: "space-around"
      }}>
        <TextField id="outlined-basic" label="Category" variant="outlined" {...register("category")}/>
        <Button variant="outlined" type={"submit"}>Add category</Button>
      </form>
    </Paper>
  )
}

import {Container, TextField} from "@mui/material";
import {useForm} from "react-hook-form";
import {useEffect} from "react";
import {axiosInstance, URLs} from "../../index";
import {getAuthHeader} from "../authentification/utils";
import {round} from "../consumption/SpentTable";

export default function LeftForm() {
    const {register, reset} = useForm({
        defaultValues: {total_consumption: 0, total_incomes: 0, left: 0}
    });

    const authHeader = getAuthHeader();

    useEffect(() => {
        axiosInstance.get(`${URLs.SPENT_ITEM}/left`, {headers: {...authHeader}}).then(res => {
            reset({
                total_consumption: round(res.data.total_consumption),
                total_incomes: round(res.data.total_incomes),
                left: round(res.data.left)
            });
        });
    }, [reset]);

    return (
        <Container>
            <TextField id="outlined-basic" variant="outlined" label="Total consumption"
                       style={{display: "flex", marginTop: "100px"}}
                       {...register("total_consumption")}/>
            <TextField id="outlined-basic" variant="outlined" label="Total incomes"
                       style={{display: "flex", marginTop: "25px"}}
                       {...register("total_incomes")}/>
            <TextField id="outlined-basic" variant="outlined" label="Total left"
                       style={{display: "flex", marginTop: "25px"}}
                       {...register("left")}/>
        </Container>
    )
}

import {Container, TextField} from "@mui/material";
import Button from "@mui/material/Button";
import {useForm} from "react-hook-form";
import {useEffect} from "react";
import {axiosInstance, URLs} from "../../index";
import {useSnackbar} from "notistack";
import {getAuthHeader} from "../authentification/utils";
import Typography from "@mui/material/Typography";
import {useDispatch, useSelector} from "react-redux";
import {setSalary} from "../slices/salaryMonthSlice";

export default function IncomeForm() {
    const {register, handleSubmit, reset} = useForm({
        defaultValues: {salary: 0, additional_payments: 0}
    });

    const dispatch = useDispatch();

    const {enqueueSnackbar} = useSnackbar();
    const authHeader = getAuthHeader();
    const date = useSelector((state) => state.consumptionDate.value);

    useEffect(() => {
        axiosInstance.get(`${URLs.INCOME}?month_date=${date}`, {headers: {...authHeader}}).then(res => {
            dispatch(setSalary(res.data?.salary));
            reset({salary: res.data.salary, additional_payments: res.data.additional_payments});
        });
    }, [reset]);

    const onSubmitIncome = data => {
        axiosInstance.post(URLs.INCOME, data, {headers: {...authHeader}}).then(res => {
            dispatch(setSalary(res.data?.salary));
            reset(res.data);
            enqueueSnackbar(`Salary ${data.salary} has been added`, {variant: "success"});
        });
    };

    return (
        <Container maxWidth="xs" style={{marginTop: "30px"}}>
            <Typography style={{display: "flex", justifyContent: "center"}}>
                Income
            </Typography>
            <form onSubmit={handleSubmit(onSubmitIncome)}
                  style={{display: "flex", flexDirection: "column", justifyContent: "space-around", height: "200px"}}>
                <TextField type="input" id="outlined-basic" label="Salary"
                           variant="outlined" {...register("salary")}/>
                <TextField type="input" id="outlined-basic" label="Additional payments"
                           variant="outlined" {...register("additional_payments")}/>
                <Button type="submit" variant="outlined">Submit</Button>
            </form>
        </Container>
    )
}

import {Autocomplete, Container, TextField} from "@mui/material";
import Button from "@mui/material/Button";
import {useState, useEffect, React} from "react";
import {axiosInstance, URLs} from "../../index";
import {useForm, Controller} from "react-hook-form";
import {useSnackbar} from "notistack";
import {getAuthHeader, logout} from "../authentification/utils";
import {useNavigate} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {addRow} from "../slices/consumpionRowsSlice";
import {AdapterDateFns} from "@mui/x-date-pickers/AdapterDateFns";
import {DesktopDatePicker} from "@mui/x-date-pickers";
import {LocalizationProvider} from "@mui/x-date-pickers/LocalizationProvider";
import {addTotal} from "../slices/totalMonthSlice";
import {addDiagramData} from "../slices/diagramDataSlice";
import {getFormattedDate} from "../slices/consumtionDateSlice";
import {setLastFive} from "../slices/lastFiveSlice";

export default function MoneySendForm() {
  const [categoryList, setCategoryList] = useState([]);
  const [date, setDate] = useState(new Date());
  const filterDate = useSelector((state) => state.consumptionDate.value);

  const {register, handleSubmit, control} = useForm();
  const {enqueueSnackbar} = useSnackbar();
  const navigation = useNavigate();
  const authHeader = getAuthHeader();

  const dispatch = useDispatch();

  useEffect(() => {
    axiosInstance.get(URLs.CATEGORIES, {headers: {...authHeader}}).then(res => {
      setCategoryList(res.data);
    }).catch(e => logout(navigation, {status: e.response.status}));
  }, []);

  const onSubmit = data => {
    const setDate = getFormattedDate(date);
    const requestBody = {amount: data.amount, "category_id": data.category.id, "create_on": setDate};

    axiosInstance.post(URLs.SPENT_ITEM, requestBody, {headers: {...authHeader}}).then(res => {
      const arrayOfDateStrings = setDate.split("-");
      const arrayOfFilterDateStrings = filterDate.split("-");

      if (setDate === filterDate) {
        dispatch(addRow(res.data));
      }
      if (arrayOfDateStrings[1] === arrayOfFilterDateStrings[1]) {
        dispatch(addDiagramData(res.data));
      }
      dispatch(addTotal(data.amount));

      axiosInstance.get(`${URLs.SPENT_ITEM}/diagram?month_date=${filterDate}`, {headers: {...authHeader}}).then(res => {
        dispatch(setLastFive(res.data.slice(0, 5)));
      });
      enqueueSnackbar("Added", {variant: "success"});
    });
  };

  return (
    <Container maxWidth="xs">
      <form onSubmit={handleSubmit(onSubmit)}
            style={{display: "flex", flexDirection: "column", justifyContent: "space-around", height: "340px"}}>
        <TextField type="input" id="outlined-basic" label="Money amount" variant="outlined" {...register("amount")}/>
        <Controller
          render={({field: {onChange, value}}) =>
            <Autocomplete
              isOptionEqualToValue={(option, value) => option.id === value.id}
              value={value}
              disablePortal
              defaultValue={null}
              getOptionLabel={option => option.category}
              options={categoryList}
              renderInput={(params) => <TextField {...params} label="Category"/>}
              onChange={(_, data) => onChange(data)}
            />
          }
          defaultValue={null}
          name="category"
          control={control}
          onChange={([, obj]) => console.log(obj)}
        />
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <DesktopDatePicker
            label="Date desktop"
            inputFormat="dd/MM/yyyy"
            value={date}
            onChange={setDate}
            renderInput={(params) => <TextField {...params} />}
          />
        </LocalizationProvider>
        <Button type="submit" variant="outlined">Submit</Button>
      </form>
    </Container>
  )
}

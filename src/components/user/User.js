import {Container, TextField} from "@mui/material";
import Button from "@mui/material/Button";
import {useForm} from "react-hook-form";
import {useEffect, useRef} from "react";
import {axiosInstance, URLs} from "../../index";
import {getAuthHeader, logout} from "../authentification/utils";
import {useNavigate} from "react-router-dom";
import {useSnackbar} from "notistack";

export default function User() {
  const {register, handleSubmit, reset, watch, formState: {errors}} = useForm({
    defaultValues: {first_name: "", last_name: "", password: "", confirmPassword: ""}
  });
  const authHeader = getAuthHeader();
  const navigation = useNavigate();
  const {enqueueSnackbar} = useSnackbar();

  const password = useRef({});
  password.current = watch("password", "");
  const passwordMessage = "Password must have at least 8 characters";

  const onSubmit = data => {
    axiosInstance.put(URLs.CURRENT_USER, data, {headers: {...authHeader}}).then(res => {
      let defaultData = {...res.data, password: "", confirmPassword: ""};
      reset(defaultData);
      enqueueSnackbar("Successfully changed", {variant: "success"});
      if (data.password) {
        logout(navigation, {needLogout: true});
      }
    }).catch(e => logout(navigation, {status: e.response.status}));
  };

  useEffect(() => {
    axiosInstance.get(URLs.CURRENT_USER, {headers: {...authHeader}}).then(res => {
      res.data.password = watch("password", "");
      reset(res.data);
    }).catch(e => logout(navigation, {status: e.response.status}));
  }, [navigation, reset]);

  return (
    <Container maxWidth={"xs"}>
      <form onSubmit={handleSubmit(onSubmit)} style={{
        height: "385px",
        display: "flex",
        alignItems: "center",
        justifyContent: "space-around",
        flexDirection: "column",
        marginTop: "20px"
      }}>
        <TextField label="First name" variant="outlined" {...register("first_name")}/>
        <TextField label="Last name" variant="outlined" {...register("last_name")}/>
        <TextField label="Password" type="password" variant="outlined" {...register("password", {
          minLength: {
            value: 8,
            message: passwordMessage
          }
        })}/>
        {errors.password && <p>{errors.password.message}</p>}
        <TextField label="Confirm password" type="password" variant="outlined" {...register("confirmPassword", {
          minLength: {
            value: 8,
            message: passwordMessage
          }, validate: value => value === password.current || "The passwords do not match"
        })}/>
        {errors.confirmPassword && <p>{errors.confirmPassword.message}</p>}
        <Button variant="outlined" type={"submit"}>Submit</Button>
      </form>
    </Container>
  );
}

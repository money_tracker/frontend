import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import {useForm} from "react-hook-form";
import {NavLink, useNavigate} from "react-router-dom";
import {axiosInstance, URLs} from "../../index";
import {useSnackbar} from "notistack";
import {additionalSnakeParams} from "./utils";

const theme = createTheme();

export default function Signin() {
  const {register, handleSubmit} = useForm();
  const {enqueueSnackbar} = useSnackbar();
  const navigate = useNavigate();

  const onSubmit = data => {
    const bodyFormData = new FormData();
    bodyFormData.append("username", data.email);
    bodyFormData.append("password", data.password);
    axiosInstance.post(URLs.LOGIN, bodyFormData, {headers: {"Content-type": "multipart/form-data"}}).then(res => {
      localStorage.setItem("token", res.data.access_token);
      enqueueSnackbar("Signed in", {
        variant: "success", ...additionalSnakeParams
      });
      navigate("/");
    }).catch(() => {
      enqueueSnackbar("Wrong email or password", {
        variant: "error", ...additionalSnakeParams
      });
    });
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline/>
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{m: 1, bgcolor: 'secondary.main'}}>
            <LockOutlinedIcon/>
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <Box component="form" onSubmit={handleSubmit(onSubmit)} noValidate sx={{mt: 1}}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              autoComplete="email"
              autoFocus
              {...register("email")}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              {...register("password")}
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary"/>}
              label="Remember me"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{mt: 3, mb: 2}}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item xs>
                <NavLink to="#" variant="body2">
                  Forgot password?
                </NavLink>
              </Grid>
              <Grid item>
                <NavLink to="/signup" variant="body2">
                  {"Don't have an account? Sign Up"}
                </NavLink>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}

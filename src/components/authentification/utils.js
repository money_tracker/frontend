import {Grow} from "@mui/material";
import {Navigate, useLocation} from "react-router-dom";

export const additionalSnakeParams = {
  anchorOrigin: {
    vertical: 'bottom',
    horizontal: 'center',
  },
  TransitionComponent: Grow
}

export default function Auth({children}) {
  const token = localStorage.getItem("token");
  let location = useLocation();
  if (!token) {
    return <Navigate to="/signin" state={{from: location}} replace/>;
  }
  return children;
}

export function getAuthHeader() {
  const token = localStorage.getItem("token");
  return {
    "Authorization": `Bearer ${token}`
  }
}

export function logout(navigation, options = {status: 0, needLogout: false}) {
  if (options.status === 401 || options.needLogout) {
    localStorage.removeItem("token");
    navigation("/");
  }
}

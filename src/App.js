import ConsumptionPage from "./components/consumption/ConsumptionPage";
import {Route, Routes} from "react-router-dom";
import Settings from "./components/settings/Settings";
import Layout from "./Layout";
import Signin from "./components/authentification/Signin";
import SignUp from "./components/authentification/Signup";
import Auth from "./components/authentification/utils";
import User from "./components/user/User";


export default function App() {
  return (
    <Routes>
      <Route path="/signin" element={<Signin/>}/>
      <Route path="/signup" element={<SignUp/>}/>
      <Route
        path="/"
        element={
          <Auth>
            <Layout/>
          </Auth>
        }>
        <Route index element={<ConsumptionPage/>}/>
        <Route path="/settings" element={<Settings/>}/>
        <Route path="/user/me" element={<User/>}/>
      </Route>
    </Routes>
  )
}

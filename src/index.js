import React from "react";
import App from "./App";
import ReactDOM from "react-dom"
import {BrowserRouter} from "react-router-dom";
import axios from "axios";
import {SnackbarProvider} from "notistack";
import {Provider} from "react-redux";
import {store} from "./store";

const baseURL = process.env.REACT_APP_API_URL;

export const axiosInstance = axios.create({
  baseURL: baseURL,
  timeout: 1000
});

export const URLs = {
  LOGIN: '/login',
  SIGNUP: '/signup',
  SPENT_ITEM: '/spent-items',
  CATEGORIES: '/categories',
  SETTINGS: '/settings',
  CURRENT_USER: '/users/me',
  INCOME: '/incomes',
};

ReactDOM.render(
  <Provider store={store}>
    <SnackbarProvider maxSnack={3}>
      <BrowserRouter>
        <App/>
      </BrowserRouter>
    </SnackbarProvider>
  </Provider>,
  document.getElementById('root')
);
